/* SimpleApp.scala */
package com.sparkfu.simple

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import scala.reflect.runtime.universe

object SimpleApp {
  def main(args: Array[String]) {
    val conf = new SparkConf(true)
      .set("spark.cassandra.connection.host", "127.0.0.1")

    val sc = new SparkContext("spark://127.0.0.1:7077", "simple", conf)
    
    // Get ALL of my things.
    val readRDD = sc.cassandraTable[(String,Integer)]("things", "mythings")
    
    // For a thing to matter it must have a value greater than zero.
    val thingsThatMatter = readRDD.filter(_._2 > 0) // A transformation
    
    // Save off the things that matter in their own table.
    thingsThatMatter.saveToCassandra("things", "thingsthatmatter", SomeColumns("key", "value"))
    
    // Fetch back the things that matter that we just wrote to Cassandra.
    val writtenRDD = sc.cassandraTable[(String,Integer)]("things", "thingsthatmatter")
    
    // Now tell the world only about the things that matter.
    writtenRDD.toArray.foreach(println)
    
    // Clean up
    // Comment this out if you'd like to validate your results inside Cassandra.
    CassandraConnector(conf).withSessionDo { session =>
      session.execute("TRUNCATE things.thingsthatmatter")
    }
    
  }
}